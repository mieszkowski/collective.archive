# -*- coding: utf-8 -*-
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Image, Paragraph, Preformatted, SimpleDocTemplate

from plone.dexterity.utils import iterSchemata
from zope.schema import getFields

from plone.app.collection.interfaces import ICollection
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.dexterity.content import Item
from plone.namedfile import field as nf
from plone import namedfile
from plone.supermodel import model
from z3c.relationfield import RelationChoice

from zope import schema
from zope.interface import implementer
from collective.archive import _

class IPdf(model.Schema):
	""" Marker interface and Dexterity Python Schema for Pdf
	"""

	collection = RelationChoice(
		title=(u"Collection to compile to PDF"),
		source=ObjPathSourceBinder(object_provides=ICollection.__identifier__),
		required=True
	)
	pdf = nf.NamedFile(
		title=(u'The PDF'),
		required=False
	)

@implementer(IPdf)
class Pdf(Item):
	"""
	"""

def compile(p, event):
	styles = getSampleStyleSheet()
	styleN = styles['Normal']
	styleH = styles['Heading1']
	styleH2 = styles['Heading2']
	text = []

	r = p.collection.to_object
	text.append(Paragraph(p.title, styleH))
	text.append(Paragraph(r.title, styleH2))
	text.append(Paragraph(r.getText(), styleN))
	for i in r.results(batch=False):
		obj = i.getObject()
		schemes = iterSchemata(obj)
		fields = {}
		for schema in schemes:
			fields.update(getFields(schema))	
		t = unicode(i.title)
		text.append(Paragraph(t,styleH))
		for j, k in fields.items():
			d = str(j)
			if hasattr(obj, d):
				text.append(Paragraph(d , styleH2))
				e = unicode(getattr(obj, d))
				if e and isinstance(e, tuple):
					text.append(Paragraph(e , styleN))
				elif hasattr(e, 'to_id'):
					text.append(Paragraph(unicode(e.to_object.title) , styleN))
				elif hasattr(e, 'isalnum'):
					text.append(Paragraph(e , styleN))
				elif e:
					text.append(Paragraph(e , styleN))
		if hasattr(obj, 'data'):
			t = unicode(i.Description())
			fmt = unicode(obj.Format())
			text.append(Paragraph(t,styleH2))
			if 'image' in fmt:
				tfn = 'tmpimage'+obj.id
				img = open (tfn, 'wb')
				img.write(obj.data) 
				img.close()
				text.append(Image(tfn,
									width=500,
									height=500,
									kind='proportional'))
			else:
			#elif ('text' in fmt) or ('application' in fmt) or ('video' in fmt):
				try: 
					text.append(Preformatted(unicode(obj.data), styleN))
				except:
					print "except some encoding does'nt work " + obj.id

	doc = SimpleDocTemplate('pdf.pdf', page='letter')
	doc.build(text)
	f = open ('pdf.pdf', 'r')
	fn = unicode(p.title) + u'.pdf'
	p.pdf = namedfile.NamedFile(f, filename = fn)
	f.close
