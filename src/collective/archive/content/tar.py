# -*- coding: utf-8 -*-
import tarfile
from plone import namedfile

from plone.dexterity.utils import iterSchemata
from zope.schema import getFields


from plone.app.collection.interfaces import ICollection
from plone.formwidget.contenttree import ObjPathSourceBinder
from z3c.relationfield import RelationChoice

from plone.dexterity.content import Item
from plone.namedfile import field as nf
from plone.supermodel import model

from zope import schema
from zope.interface import implementer
from collective.archive import _


class ITar(model.Schema):
	""" Marker interface and Dexterity Python Schema for Tar
	"""
	collection = RelationChoice(
		title=(u"Collection to archive to Tar"),
		source=ObjPathSourceBinder(object_provides=ICollection.__identifier__),
		required=False
	)
	tar = nf.NamedFile(
		title=(u'The Tar archive'),
		required=False
	)


@implementer(ITar)
class Tar(Item):
	"""
	"""

def archive(t, event):
	fn = str(t.title) + u'.tar'
	q = open(fn, 'w+b')
	z = tarfile.TarFile(name=fn,mode='w', fileobj=q)
	r = t.collection.to_object
	for i in r.results(batch=False):
		obj = i.getObject()
		if hasattr(obj, 'data'):
			tfn = obj.getFilename()
			if tfn:
				tf = open (tfn, 'wb')
			else: tf = open (obj.id, 'wb')
			tf.write(obj.data) 
		else:
			tfn = obj.id
			tf = open (tfn, 'w')
			tf.write(i.title)
			schemes = iterSchemata(obj)
			fields = {}
			for schema in schemes:
				fields.update(getFields(schema))
			for j,k in fields.items():
				if hasattr(obj, str(j)):
					e = getattr (obj, str(j))
					tf.write(str(j))
					if e and isinstance(e, tuple):
						tf.write(str(e))
					elif hasattr(e, 'to_id'):
						tf.write(str(e.to_object.title))
					elif hasattr(e, 'isalnum'):
						tf.write(str(e))
					elif e:
						tf.write(str(e))
		tf.close()
		if tfn:
			z.add(name=tfn)
		else: z.add(name=obj.id)
	z.close()
	t.tar = namedfile.NamedFile(q, filename = fn)
	q.close()

