# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from zope.publisher.interfaces.browser import IDefaultBrowserLayer

from zope.interface import Interface
from plone.app.blob.interfaces import IATBlobFile

class ICollectiveArchiveLayer(IDefaultBrowserLayer):
    """Marker interface that defines a browser layer."""

class IArchive(IATBlobFile):
	"""interface for an adapter from ICollection"""
	def __call__(self):
		"""Collection to tar archive"""


class ICompile(IATBlobFile):
	def __call__(self):
		"""Collection to pdf"""

