# -*- coding: utf-8 -*-
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import collective.archive


class CollectiveArchiveLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.app.dexterity
        self.loadZCML(package=plone.app.dexterity)
        self.loadZCML(package=collective.archive)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'collective.archive:default')


COLLECTIVE_ARCHIVE_FIXTURE = CollectiveArchiveLayer()


COLLECTIVE_ARCHIVE_INTEGRATION_TESTING = IntegrationTesting(
    bases=(COLLECTIVE_ARCHIVE_FIXTURE,),
    name='CollectiveArchiveLayer:IntegrationTesting',
)


COLLECTIVE_ARCHIVE_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(COLLECTIVE_ARCHIVE_FIXTURE,),
    name='CollectiveArchiveLayer:FunctionalTesting',
)


COLLECTIVE_ARCHIVE_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        COLLECTIVE_ARCHIVE_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name='CollectiveArchiveLayer:AcceptanceTesting',
)
