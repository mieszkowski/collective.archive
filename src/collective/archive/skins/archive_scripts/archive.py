## Script (Python) "archive"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=Archive Collection content to tar file

from plone.api.portal import create
import tarfile

REQUEST = context.REQUEST
z = tarfile.TarFile(name=context['Title'],mode='w')
for i in context.items():
	t = i['Title']
	d = i['Description']
	f = i['file']
	if (f):
		y = z.gettarinfo(t, fileobj=f)
		z.addfile(tarinfo=y, fileobj=f)
	elif (getattr(object, "portal_type", "") == "Folder"):
		pass
	else:
		y = z.gettarinfo(t)
		z.addfile(tarinfo=y)
portal = plone.api.portal.get()
obj = create(container=portal, type="File", id=t, name=t, file=z)
z.close()
