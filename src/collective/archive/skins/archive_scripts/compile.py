## Script (Python) "compile"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##parameters=
##title=Compile a pdf from Collection

from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import Paragraph, Frame, SimpleDocTemplate

from plone.api.portal import create

REQUEST = context.REQUEST

styles = getSampleStyleSheet()
styleN = styles['Normal']
styleH = styles['Heading1']
doc = []
c = Canvas('pdf.pdf')
r = Frame(inch, inch, 6*inch, 9*inch, showBoundary=1)
for i in context.items():
	t = i['Title']
	d = i['Description']
	f = i['file']
	doc.append(Paragraph(t,styleH))
	doc.append(Paragraph(d,styleN))
	if (f):
		s = str(f.read())
		doc.append(s)
		r.addFromList(doc,c)
	else:
		#TODO for attribut in attributes doc.append(attribut
		r.addFromList(doc,c)
