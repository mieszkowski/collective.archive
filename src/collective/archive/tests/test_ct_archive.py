# -*- coding: utf-8 -*-
from collective.archive.content.archive import IArchive  # NOQA E501
from collective.archive.testing import COLLECTIVE_ARCHIVE_INTEGRATION_TESTING  # noqa
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


try:
    from plone.dexterity.schema import portalTypeToSchemaName
except ImportError:
    # Plone < 5
    from plone.dexterity.utils import portalTypeToSchemaName


class ArchiveIntegrationTest(unittest.TestCase):

    layer = COLLECTIVE_ARCHIVE_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_schema(self):
        fti = queryUtility(IDexterityFTI, name='Archive')
        schema = fti.lookupSchema()
        self.assertEqual(IArchive, schema)

    def test_fti(self):
        fti = queryUtility(IDexterityFTI, name='Archive')
        self.assertTrue(fti)

    def test_factory(self):
        fti = queryUtility(IDexterityFTI, name='Archive')
        factory = fti.factory
        obj = createObject(factory)

        self.assertTrue(
            IArchive.providedBy(obj),
            u'IArchive not provided by {0}!'.format(
                obj,
            ),
        )

    def test_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.portal,
            type='Archive',
            id='archive',
        )
        self.assertTrue(
            IArchive.providedBy(obj),
            u'IArchive not provided by {0}!'.format(
                obj.id,
            ),
        )
