# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s collective.archive -t test_pusharchive.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src collective.archive.testing.COLLECTIVE_ARCHIVE_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot src/plonetraining/testing/tests/robot/test_pusharchive.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a PushArchive
  Given a logged-in site administrator
    and an add PushArchive form
   When I type 'My PushArchive' into the title field
    and I submit the form
   Then a PushArchive with the title 'My PushArchive' has been created

Scenario: As a site administrator I can view a PushArchive
  Given a logged-in site administrator
    and a PushArchive 'My PushArchive'
   When I go to the PushArchive view
   Then I can see the PushArchive title 'My PushArchive'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add PushArchive form
  Go To  ${PLONE_URL}/++add++PushArchive

a PushArchive 'My PushArchive'
  Create content  type=PushArchive  id=my-pusharchive  title=My PushArchive


# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form-widgets-IBasic-title  ${title}

I submit the form
  Click Button  Save

I go to the PushArchive view
  Go To  ${PLONE_URL}/my-pusharchive
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a PushArchive with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the PushArchive title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
