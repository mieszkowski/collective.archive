# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s collective.archive -t test_pdf.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src collective.archive.testing.COLLECTIVE_ARCHIVE_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot src/plonetraining/testing/tests/robot/test_pdf.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a PDF
  Given a logged-in site administrator
    and an add PDF form
   When I type 'My PDF' into the title field
    and I submit the form
   Then a PDF with the title 'My PDF' has been created

Scenario: As a site administrator I can view a PDF
  Given a logged-in site administrator
    and a PDF 'My PDF'
   When I go to the PDF view
   Then I can see the PDF title 'My PDF'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add PDF form
  Go To  ${PLONE_URL}/++add++PDF

a PDF 'My PDF'
  Create content  type=PDF  id=my-pdf  title=My PDF


# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form-widgets-IBasic-title  ${title}

I submit the form
  Click Button  Save

I go to the PDF view
  Go To  ${PLONE_URL}/my-pdf
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a PDF with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the PDF title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
