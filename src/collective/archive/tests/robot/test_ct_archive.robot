# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s collective.archive -t test_archive.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src collective.archive.testing.COLLECTIVE_ARCHIVE_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot src/plonetraining/testing/tests/robot/test_archive.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a Archive
  Given a logged-in site administrator
    and an add Archive form
   When I type 'My Archive' into the title field
    and I submit the form
   Then a Archive with the title 'My Archive' has been created

Scenario: As a site administrator I can view a Archive
  Given a logged-in site administrator
    and a Archive 'My Archive'
   When I go to the Archive view
   Then I can see the Archive title 'My Archive'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add Archive form
  Go To  ${PLONE_URL}/++add++Archive

a Archive 'My Archive'
  Create content  type=Archive  id=my-archive  title=My Archive


# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form-widgets-IBasic-title  ${title}

I submit the form
  Click Button  Save

I go to the Archive view
  Go To  ${PLONE_URL}/my-archive
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a Archive with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the Archive title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
