# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s collective.archive -t test_tar.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src collective.archive.testing.COLLECTIVE_ARCHIVE_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot src/plonetraining/testing/tests/robot/test_tar.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a Tar
  Given a logged-in site administrator
    and an add Tar form
   When I type 'My Tar' into the title field
    and I submit the form
   Then a Tar with the title 'My Tar' has been created

Scenario: As a site administrator I can view a Tar
  Given a logged-in site administrator
    and a Tar 'My Tar'
   When I go to the Tar view
   Then I can see the Tar title 'My Tar'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add Tar form
  Go To  ${PLONE_URL}/++add++Tar

a Tar 'My Tar'
  Create content  type=Tar  id=my-tar  title=My Tar


# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form-widgets-IBasic-title  ${title}

I submit the form
  Click Button  Save

I go to the Tar view
  Go To  ${PLONE_URL}/my-tar
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a Tar with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the Tar title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
