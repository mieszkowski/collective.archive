from zope.component import adapter
from zope.interface import implementer

from collective.archive.interfaces import IArchive, ICompile
from plone.app.collection.interfaces import ICollection
from AccessControl import ClassSecurityInfo
from AccessControl.class_init import InitializeClass
from Acquisition import Implicit

from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import Paragraph, Frame, SimpleDocTemplate

#from plone.api.portal import create

import tarfile

@implementer(IArchive)
class Archive(object):

	def __call__(self):
		z = tarfile.TarFile(name=context['Title'],mode='w')
		for i in context.items():
			t = i['Title']
			d = i['Description']
			f = i['file']
			if (f):
				y = z.gettarinfo(t, fileobj=f)
				z.addfile(tarinfo=y, fileobj=f)
#			elif (getattr(object, "portal_type", "") == "Folder"

			else:
				y = z.gettarinfo(t)
				z.addfile(tarinfo=y)
		z.close()

@implementer(ICompile)
class Compile(object):

	def __call__(self):
		styles = getSampleStyleSheet()
		styleN = styles['Normal']
		styleH = styles['Heading1']
		doc = []
		c = Canvas('pdf.pdf')
		r = Frame(inch, inch, 6*inch, 9*inch, showBoundary=1)
		for i in context.items():
			t = i['Title']
			d = i['Description']
			f = i['file']
			doc.append(Paragraph(t,styleH))
			doc.append(Paragraph(d,styleN))
			if (f):
				s = str(f.read())
				doc.append(s)
				r.addFromList(doc,c)
			else:
				#TODO for attribut in attributes doc.append(attribut
				r.addFromList(doc,c)
				
