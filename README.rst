.. This README is meant for consumption by humans and pypi. Pypi can render rst files so please do not use Sphinx features.
   If you want to learn more about writing documentation, please check out: http://docs.plone.org/about/documentation_styleguide.html
   This text does not appear on pypi or github. It is a comment.

==================
collective.archive
==================

Archive and PDF compiling support for Plone. 

Features
--------

- Content type "Archive"  untars a tar file to a container.

- Content type "PDF" compiles a PDF from a collection.

- Content type "Tar" archives a collection.


Examples
-------
 

Documentation
-------------

Template provided by bobtemplates.plone.

Written and tested on Plone 4.3.19

Installation
------------

Install collective.archive by getting it from git and adding it to your buildout::

    [buildout]

    ...

    eggs =
        collective.archive

	develop =
		src/collective.archive

and then running ``bin/buildout``


Contribute
----------

- Issue Tracker:
- Source Code:https://bitbucket.org/mieszkowski/collective.archive/src/master/


Support
-------

If you are having issues, please let us know.


License
-------

The project is licensed under the GPLv2.
